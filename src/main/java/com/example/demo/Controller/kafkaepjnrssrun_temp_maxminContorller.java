package com.example.demo.Controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.json.JsonParser;
import org.springframework.boot.json.JsonParserFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class kafkaepjnrssrun_temp_maxminContorller {

    @Service
    public class kafkaepController {

        @Autowired
        @Qualifier("entityManagerFactoryPrimary")
        private EntityManager mysqlPrimary;

        @Autowired
        @Qualifier("entityManagerFactorySecondary")
        private EntityManager mysqlSecondary;


        @Autowired
        private JdbcTemplate jdbcTemplate;

        @PersistenceContext
        private EntityManager em;


        // private final Logger logger = LoggerFactory.getLogger(Producer.class);
        @Autowired
        private final ObjectMapper mapper = new ObjectMapper();


        @KafkaListener(topics = "jnrsSRUN", groupId = "abcdtestep")//local
        //   @KafkaListener(topics = "jnrsSRUN", groupId = "abcdtestcloudep")//could
        public void consume(String message) throws IOException {
            //logger.info(String.format("%s", message));



            String sensorId = "";
            String exportTime = "";
            String plant = "mah";
            String factory = "Factory2";
            String tTemp1 = "";





            try {
                JsonParser springParser = JsonParserFactory.getJsonParser();
                Map<String, Object> map = springParser.parseMap(message);
                String[] mapArray = new String[map.size()];
                //System.out.println("Items found: " + mapArray.length);
                int i = 0;
                for (Map.Entry<String, Object> entry : map.entrySet()) {
                    /*印出所有值   */
                    //  System.out.println(entry.getKey() + " = " + entry.getValue());
                    i++;


                    try {
                        sensorId = map.get("sensor_id").toString();
                        exportTime = map.get("time").toString();
                        tTemp1 = map.get("temperature").toString();


                    } catch (Exception ex) {
                        System.out.println("Exception to get the Json value");
                    }


                }




//            Primary
//            更新最新表格energy_max_min_mapping資料表
//            搜尋相同ＩＤ是否有資料
                String sql = "Select * from mah_test.energy_max_min_mapping where tempId='" + sensorId + "' ";
                List<HashMap<String, Object>> result = new ArrayList<>();


                //單機是  Query query = em.createNativeQuery(sql);
                //現在是多台伺服器因此需要改mysqlPrimary＆mysqlSecondary
                try {
                    Query query = mysqlPrimary.createNativeQuery(sql);
                    // Query 介面是 spring-data-jpa 的介面，而 SQLQuery 介面是 hibenate 的介面，這裡的做法就是先轉成 hibenate 的查詢介面物件，然後設定結果轉換器
                    query.unwrap(SQLQuery.class).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
                    result = query.getResultList();

                } catch (Exception e) {
                    //throw new Exception(e.getMessage());
                    System.out.println(e.getMessage());
                }


                String sql2 = "Select sensorId from mah_test.energy_tempid_sensors_mapping where tempId='" + sensorId + "' ";
                List<HashMap<String, Object>> result2 = new ArrayList<>();


                //單機是  Query query = em.createNativeQuery(sql);
                //現在是多台伺服器因此需要改mysqlPrimary＆mysqlSecondary
                try {
                    Query query2 = mysqlPrimary.createNativeQuery(sql2);
                    // Query 介面是 spring-data-jpa 的介面，而 SQLQuery 介面是 hibenate 的介面，這裡的做法就是先轉成 hibenate 的查詢介面物件，然後設定結果轉換器
                    query2.unwrap(SQLQuery.class).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
                    result2 = query2.getResultList();

                } catch (Exception e) {
                    //throw new Exception(e.getMessage());
                    System.out.println(e.getMessage());
                }




                for (Object tmp2 : result2) {


                    HashMap<String, Object> datatempmapping = (HashMap<String, Object>) tmp2;
                    datatempmapping.get("sensorId");
                    String sensorId2= (String)datatempmapping.get("sensorId");

//                    System.out.println(sensorId2);

                    for (Object tmp : result  )  {


                        //溫度
                        double sruntemp1 = Double.parseDouble(tTemp1);
                        HashMap<String, Object> datatempmax = (HashMap<String, Object>) tmp;
                        datatempmax.get("tMax");
                        double tempmax = (double) datatempmax.get("tMax");

                        //測試值

//                        System.out.println(tempmax);
//                        System.out.println(sruntemp1);




                        //溫度

                        if (sruntemp1 > tempmax) {


                            String epupdatetemp1 = "update mah_test.energy_warming_now set databaseTime=now(), exportTime='" + exportTime + "'," +
                                    " plant='" + plant + "', factory='" + factory + "' , " +
                                    "   tStatus='2' " +

                                    "  where sensorId= '" + sensorId2 + "'";
                            jdbcTemplate.execute(epupdatetemp1);

                            System.out.println(" Temp1 abnormal_high " + sensorId2 + " "+sruntemp1+" ");

                            //另外存energy_alarm_temp_history
                            String epinsertalarmtemp1 = "insert mah_test.energy_alarm_temp_history (plant,factory,sensorId,exportTime,tStatus,temp) " +
                                    "values('"+plant+"','"+factory+"','"+sensorId2+"','" + exportTime + "','2','"+sruntemp1+"')";

                            jdbcTemplate.execute(epinsertalarmtemp1);

                            System.out.println(" Temp1 abnormal_high  insert energy_alarm_temp1_history ");



                        } else {


                            String epupdatetemp1_2 = "update mah_test.energy_warming_now set databaseTime=now(), exportTime='" + exportTime + "'," +
                                    " plant='" + plant + "', factory='" + factory + "' , " +
                                    "   tStatus='1' " +

                                    "  where sensorId= '" + sensorId2 + "'";
                            jdbcTemplate.execute(epupdatetemp1_2);
                            System.out.println(" Temp1 normall " + sensorId2 + "  "+sruntemp1+" ");
                        }

                    }


                }





            } catch (Exception e) {
                System.out.println(e.getMessage());
            }


        }

    }
}
