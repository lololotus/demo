package com.example.demo.Controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.hibernate.SQLQuery;
import org.hibernate.transform.Transformers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.json.JsonParser;
import org.springframework.boot.json.JsonParserFactory;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class kafkaepsrunmaxminContorller {

        @Autowired
        @Qualifier("entityManagerFactoryPrimary")
        private EntityManager mysqlPrimary;

        @Autowired
        @Qualifier("entityManagerFactorySecondary")
        private EntityManager mysqlSecondary;


        @Autowired
        private JdbcTemplate jdbcTemplate;

        @PersistenceContext
        private EntityManager em;


        // private final Logger logger = LoggerFactory.getLogger(Producer.class);
        @Autowired
        private final ObjectMapper mapper = new ObjectMapper();


        @KafkaListener(topics = "epSRUN", groupId = "abcdtestep")//local
        //   @KafkaListener(topics = "epSRUN", groupId = "abcdtestcloudep")//could
        public void consume(String message) throws IOException {
            //logger.info(String.format("%s", message));



            String sensorId = "";
            String exportTime = "";
            String plant = "mah";
            String factory = "Factory2";
            String v1 = "";
            String i1 = "";
           // String tTemp1 = "";
            String v2 = "";
            String i2 = "";
            String v3 = "";
            String i3 = "";



            try {
                JsonParser springParser = JsonParserFactory.getJsonParser();
                Map<String, Object> map = springParser.parseMap(message);
                String[] mapArray = new String[map.size()];
                //System.out.println("Items found: " + mapArray.length);
                int i = 0;
                for (Map.Entry<String, Object> entry : map.entrySet()) {
                    /*印出所有值   */
                   //  System.out.println(entry.getKey() + " = " + entry.getValue());
                    i++;


                    try {
                        sensorId = map.get("sensor_id").toString();
                        exportTime = map.get("time").toString();
                        v1 = map.get("vab").toString();
                        i1 = map.get("ia").toString();
                    //    tTemp1 = map.get("ttemp1").toString();
                        v2 = map.get("vbc").toString();
                        i2 = map.get("ib").toString();
                        v3 = map.get("vca").toString();
                        i3 = map.get("ic").toString();


                    } catch (Exception ex) {
                        System.out.println("Exception to get the Json value");
                    }


                }

                System.out.println(i1);
                System.out.println(i2);
                System.out.println(i3);


                String sensorId2 = ("sensorId" + sensorId);
                String changeutc = "select date_add('" + exportTime + "' , interval 7 hour)";


//            Primary
//            更新最新表格energy_max_min_mapping資料表
//            搜尋相同ＩＤ是否有資料
                String sql = "Select * from mah_test.energy_max_min_mapping where sensorId='" + sensorId + "' ";
                List<HashMap<String, Object>> result = new ArrayList<>();


                //單機是  Query query = em.createNativeQuery(sql);
                //現在是多台伺服器因此需要改mysqlPrimary＆mysqlSecondary
                try {
                    Query query = mysqlPrimary.createNativeQuery(sql);
                    // Query 介面是 spring-data-jpa 的介面，而 SQLQuery 介面是 hibenate 的介面，這裡的做法就是先轉成 hibenate 的查詢介面物件，然後設定結果轉換器
                    query.unwrap(SQLQuery.class).setResultTransformer(Transformers.ALIAS_TO_ENTITY_MAP);
                    result = query.getResultList();

                } catch (Exception e) {
                    //throw new Exception(e.getMessage());
                    System.out.println(e.getMessage());
                }

//                String epresult = result.toString();
//                String epresult2 = epresult.replace("]", "").replace("[", "").replace(" ", "");
//                System.out.println(epresult2);


                for (Object tmp : result) {

                    //電壓
                    double srunv1 = Double.parseDouble(v1);
                    double srunv2 = Double.parseDouble(v2);
                    double srunv3 = Double.parseDouble(v3);
                    HashMap<String, Object> datavmax = (HashMap<String, Object>) tmp;
                    datavmax.get("vMax");
                    double vmax = (double) datavmax.get("vMax");

                    HashMap<String, Object> datavmin = (HashMap<String, Object>) tmp;
                    datavmin.get("vMin");
                    double vmin = (double) datavmin.get("vMin");


                    //電流
                    double sruni1 = Double.parseDouble(i1);
                    double sruni2 = Double.parseDouble(i2);
                    double sruni3 = Double.parseDouble(i3);
                    HashMap<String, Object> dataimax = (HashMap<String, Object>) tmp;
                    dataimax.get("iMax");
                    double imax = (double) dataimax.get("iMax");

//                    //溫度
////                    double sruntemp1 = Double.parseDouble(tTemp1);
////                    HashMap<String, Object> datatempmax = (HashMap<String, Object>) tmp;
////                    datatempmax.get("tMax");
////                    double tempmax = (double) datatempmax.get("tMax");
//
//                    //測試值
//                    System.out.println(vmax);
//                    System.out.println(vmin);
//                    System.out.println(srunv1);
//                    System.out.println(srunv2);
//                    System.out.println(srunv3);
//
//                    System.out.println(imax);
//                    System.out.println(sruni1);
//                    System.out.println(sruni2);
//                    System.out.println(sruni3);

////                    System.out.println(tempmax);
////                    System.out.println(sruntemp1);
//
//
                    //電壓max min v1-v3
                    //電壓v1 max
                    if (srunv1 > vmax) {
                        //電壓過高
                        String epupdatev1 = "update mah_test.energy_warming_now set databaseTime=now(), exportTime='" + exportTime + "'," +
                                " plant='" + plant + "', factory='" + factory + "' , " +
                                "   v1Status='2' " +

                                "  where sensorId= '" + sensorId + "'";
                        jdbcTemplate.execute(epupdatev1);

                        System.out.println(" V1 abnormal_high " + sensorId + "  '"+srunv1+"'");

                        //另外存energy_alarm_v1_history
                        String epinsertalarmv1 = "insert mah_test.energy_alarm_v1_history (plant,factory,sensorId,exportTime,v1Status,v1) " +
                                "values('"+plant+"','"+factory+"','"+sensorId+"','" + exportTime + "','2','"+srunv1+"')";

                        jdbcTemplate.execute(epinsertalarmv1);

                        System.out.println(" V1 abnormal_high  insert energy_alarm_v1_history ");


                    }
                    //電壓v1 min
                    else if (srunv1 < vmin) {
                        //電壓過低
                        String epupdatev1_min = "update mah_test.energy_warming_now set databaseTime=now(), exportTime='" + exportTime + "'," +
                                " plant='" + plant + "', factory='" + factory + "' , " +
                                "   v1Status='-1' " +

                                "  where sensorId= '" + sensorId + "'";
                        jdbcTemplate.execute(epupdatev1_min);

                        System.out.println(" V1 abnormal_low " + sensorId + " '"+srunv1+"' ");

                        //另外存energy_alarm_v1_history
                        String epinsertalarmv1_2 = "insert mah_test.energy_alarm_v1_history (plant,factory,sensorId,exportTime,v1Status,v1) " +
                                "values('"+plant+"','"+factory+"','"+sensorId+"','" + exportTime + "','-1','"+srunv1+"')";

                        jdbcTemplate.execute(epinsertalarmv1_2);

                        System.out.println(" V1 abnormal_low  insert energy_alarm_v1_history ");


                    } else {

                        String epupdatev1_2 = "update mah_test.energy_warming_now set databaseTime=now(), exportTime='" + exportTime + "'," +
                                " plant='" + plant + "', factory='" + factory + "' , " +
                                "   v1Status='1' " +

                                "  where sensorId= '" + sensorId + "'";
                        jdbcTemplate.execute(epupdatev1_2);

                        System.out.println(" V1 normall " + sensorId + " '"+srunv2+"'");
                    }

                    //   電壓v2 max
                    if (srunv2 > vmax) {
                        String epupdatev2 = "update mah_test.energy_warming_now set databaseTime=now(), exportTime='" + exportTime + "'," +
                                " plant='" + plant + "', factory='" + factory + "' , " +
                                "   v2Status='2' " +

                                "  where sensorId= '" + sensorId + "'";
                        jdbcTemplate.execute(epupdatev2);

                        System.out.println(" V2 abnormal_high " + sensorId + " ");

                        //另外存energy_alarm_v2_history
                        String epinsertalarmv2 = "insert mah_test.energy_alarm_v2_history (plant,factory,sensorId,exportTime,v2Status,v2) " +
                                "values('"+plant+"','"+factory+"','"+sensorId+"','" + exportTime + "','2','"+srunv2+"')";

                        jdbcTemplate.execute(epinsertalarmv2);

                        System.out.println(" V2 abnormal_high  insert energy_alarm_v2_history ");


                    }
                    //電壓v2 min
                    else if (srunv2 < vmin) {
                        String epupdatev2_min = "update mah_test.energy_warming_now set databaseTime=now(), exportTime='" + exportTime + "'," +
                                " plant='" + plant + "', factory='" + factory + "' , " +
                                "   v2Status='-1' " +

                                "  where sensorId= '" + sensorId + "'";
                        jdbcTemplate.execute(epupdatev2_min);

                        System.out.println(" V2 abnormal_low " + sensorId + " '"+srunv2+"' ");

                        //另外存energy_alarm_v2_history
                        String epinsertalarmv2 = "insert mah_test.energy_alarm_v2_history (plant,factory,sensorId,exportTime,v2Status,v2) " +
                                "values('"+plant+"','"+factory+"','"+sensorId+"','" + exportTime + "','-1','"+sruni2+"')";

                        jdbcTemplate.execute(epinsertalarmv2);

                        System.out.println(" V2 abnormal_low   insert energy_alarm_v2_history ");

                    } else {

                        String epupdatev2_2 = "update mah_test.energy_warming_now set databaseTime=now(), exportTime='" + exportTime + "'," +
                                " plant='" + plant + "', factory='" + factory + "' , " +
                                "   v2Status='1' " +

                                "  where sensorId= '" + sensorId + "'";
                        jdbcTemplate.execute(epupdatev2_2);

                        System.out.println(" V2 normall " + sensorId + " '"+srunv2+"'");
                    }


                    //電壓v3 max
                    if (srunv3 > vmax) {
                        String epupdatev3 = "update mah_test.energy_warming_now set databaseTime=now(), exportTime='" + exportTime + "'," +
                                " plant='" + plant + "', factory='" + factory + "' , " +
                                "   v3Status='2' " +

                                "  where sensorId= '" + sensorId + "'";
                        jdbcTemplate.execute(epupdatev3);

                        System.out.println(" V3 abnormal_high " + sensorId + " '"+srunv3+"' ");

                        //另外存energy_alarm_v3_history
                        String epinsertalarmv3 = "insert mah_test.energy_alarm_v3_history (plant,factory,sensorId,exportTime,v3Status,v3) " +
                                "values('"+plant+"','"+factory+"','"+sensorId+"','" + exportTime + "','2','"+srunv3+"')";

                        jdbcTemplate.execute(epinsertalarmv3);

                        System.out.println(" V3 abnormal_high  insert energy_alarm_v3_history ");


                    }
                    //電壓v3 min
                    else if (srunv3 < vmin) {
                        String epupdatev3_min = "update mah_test.energy_warming_now set databaseTime=now(), exportTime='" + exportTime + "'," +
                                " plant='" + plant + "', factory='" + factory + "' , " +
                                "   v3Status='-1' " +

                                "  where sensorId= '" + sensorId + "'";
                        jdbcTemplate.execute(epupdatev3_min);

                        System.out.println(" V3 abnormal_low " + sensorId + " '"+srunv3+"' ");


                        //另外存energy_alarm_v3_history
                        String epinsertalarmv3 = "insert mah_test.energy_alarm_v3_history (plant,factory,sensorId,exportTime,v3Status,v3) " +
                                "values('"+plant+"','"+factory+"','"+sensorId+"','" + exportTime + "','-1','"+srunv3+"')";

                        jdbcTemplate.execute(epinsertalarmv3);

                        System.out.println(" V3 abnormal_low  insert energy_alarm_v3_history ");
                    } else {

                        String epupdatev3_2 = "update mah_test.energy_warming_now set databaseTime=now(), exportTime='" + exportTime + "'," +
                                " plant='" + plant + "', factory='" + factory + "' , " +
                                "   v3Status='1' " +

                                "  where sensorId= '" + sensorId + "'";
                        jdbcTemplate.execute(epupdatev3_2);

                        System.out.println(" V3 normall " + sensorId + " '"+srunv3+"'");
                    }


                    //電流max i1

                    if (sruni1 > imax) {
                        String epupdatei1 = "update mah_test.energy_warming_now set databaseTime=now(), exportTime='" + exportTime + "'," +
                                " plant='" + plant + "', factory='" + factory + "' , " +
                                "   i1Status='2' " +

                                "  where sensorId= '" + sensorId + "'";
                        jdbcTemplate.execute(epupdatei1);

                        System.out.println(" I1 abnormal_high " + sensorId + " '"+sruni1+"' ");

                        //另外存energy_alarm_i1_history
                        String epinsertalarmi1 = "insert mah_test.energy_alarm_i1_history (plant,factory,sensorId,exportTime,i1Status,i1) " +
                                "values('"+plant+"','"+factory+"','"+sensorId+"','" + exportTime + "','2','"+sruni1+"')";

                        jdbcTemplate.execute(epinsertalarmi1);

                        System.out.println(" I1 abnormal_high  insert energy_alarm_i1_history ");


                    } else {


                        String epupdatei1_2 = "update mah_test.energy_warming_now set databaseTime=now(), exportTime='" + exportTime + "'," +
                                " plant='" + plant + "', factory='" + factory + "' , " +
                                "   i1Status='1' " +

                                "  where sensorId= '" + sensorId + "'";
                        jdbcTemplate.execute(epupdatei1_2);
                        System.out.println(" I1 normall " + sensorId + " '"+sruni1+"'");
                    }


                    //電流max i2

                    if (sruni2 > imax) {
                        String epupdatei2 = "update mah_test.energy_warming_now set databaseTime=now(), exportTime='" + exportTime + "'," +
                                " plant='" + plant + "', factory='" + factory + "' , " +
                                "   i2Status='2' " +

                                "  where sensorId= '" + sensorId + "'";
                        jdbcTemplate.execute(epupdatei2);

                        System.out.println(" I2 abnormal_high " + sensorId + "  '"+sruni2+"'");

                        //另外存energy_alarm_i2_history
                        String epinsertalarmi2 = "insert mah_test.energy_alarm_i2_history (plant,factory,sensorId,exportTime,i2Status,i2) " +
                                "values('"+plant+"','"+factory+"','"+sensorId+"','" + exportTime + "','2','"+sruni2+"')";

                        jdbcTemplate.execute(epinsertalarmi2);

                        System.out.println(" I2 abnormal_high  insert energy_alarm_i2_history ");



                    } else {


                        String epupdatei2_2 = "update mah_test.energy_warming_now set databaseTime=now(), exportTime='" + exportTime + "'," +
                                " plant='" + plant + "', factory='" + factory + "' , " +
                                "   i2Status='1' " +

                                "  where sensorId= '" + sensorId + "'";
                        jdbcTemplate.execute(epupdatei2_2);
                        System.out.println(" I2 normall " + sensorId + " '"+sruni2+"'");
                    }

                    //電流max i3

                    if (sruni3 > imax) {
                        String epupdatei1 = "update mah_test.energy_warming_now set databaseTime=now(), exportTime='" + exportTime + "'," +
                                " plant='" + plant + "', factory='" + factory + "' , " +
                                "   i3Status='2' " +

                                "  where sensorId= '" + sensorId + "'";
                        jdbcTemplate.execute(epupdatei1);

                        System.out.println(" I3 abnormal_high " + sensorId + "  '"+sruni3+"'");

                        //另外存energy_alarm_i3_history
                        String epinsertalarmi3 = "insert mah_test.energy_alarm_i3_history (plant,factory,sensorId,exportTime,i3Status,i3) " +
                                "values('"+plant+"','"+factory+"','"+sensorId+"','" + exportTime + "','2','"+sruni3+"')";

                        jdbcTemplate.execute(epinsertalarmi3);

                        System.out.println(" I3 abnormal_high  insert energy_alarm_i3_history ");


                    } else {


                        String epupdatei3_2 = "update mah_test.energy_warming_now set databaseTime=now(), exportTime='" + exportTime + "'," +
                                " plant='" + plant + "', factory='" + factory + "' , " +
                                "   i3Status='1' " +

                                "  where sensorId= '" + sensorId + "'";
                        jdbcTemplate.execute(epupdatei3_2);
                        System.out.println(" I3 normall " + sensorId + " '"+sruni3+"'");
                    }


//                    //溫度
//
////                    if (sruntemp1 > tempmax) {
////                        String epupdatetemp1 = "update mah_test.energy_warming_now set databaseTime=now(), exportTime='" + exportTime + "'," +
////                                " plant='" + plant + "', factory='" + factory + "' , " +
////                                "   tStatus='2' " +
////
////                                "  where sensorId= '" + sensorId + "'";
////                        jdbcTemplate.execute(epupdatetemp1);
////
////                        System.out.println(" Temp1 abnormal_high " + sensorId + " ");
////
////                        //另外存energy_alarm_temp_history
////                        String epinsertalarmtemp1 = "insert mah_test.energy_alarm_temp_history (plant,factory,sensorId,exportTime,tStatus) " +
////                                "values('"+plant+"','"+factory+"','"+sensorId+"','" + exportTime + "','2')";
////
////                        jdbcTemplate.execute(epinsertalarmtemp1);
////
////                        System.out.println(" Temp1 abnormal_high  insert energy_alarm_temp1_history ");
////
////
////
////                    } else {
////
////
////                        String epupdatetemp1_2 = "update mah_test.energy_warming_now set databaseTime=now(), exportTime='" + exportTime + "'," +
////                                " plant='" + plant + "', factory='" + factory + "' , " +
////                                "   tStatus='1' " +
////
////                                "  where sensorId= '" + sensorId + "'";
////                        jdbcTemplate.execute(epupdatetemp1_2);
////                        System.out.println(" Temp1 normall " + sensorId + "");
////                    }

                }



            } catch (Exception e) {
                System.out.println(e.getMessage());
            }


        }

    }

